package elimex;

import java.net.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Elimex {

    static int prod;
    static int p;
    static int count = 1;
    static String match1 = "";
    static String match2 = "";
    static String match3 = "";

    static LinkedHashMap<String, String> R_NEWFILE = new LinkedHashMap<>();
    static StringBuilder row = new StringBuilder();

    public static void main(String[] args) throws MalformedURLException, IOException {
        for (prod = 1265; prod <= 1270; prod++) {//75, 1188
            for (p = 1; p < 12; p++) {
                URL elimex = new URL("https://elimex.hu/gyartokategoriak/" + prod + "?list&page=" + p);
                BufferedReader in;
                try {
                    in = new BufferedReader(
                            new InputStreamReader(elimex.openStream()));
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        Pattern pattern1 = Pattern.compile(
                                "(?<=>)[A-Z\\d-+]+(?=</a></span><b)");
                        Matcher matcher1 = pattern1.matcher(inputLine);
                        if (matcher1.find()) {
                            match1 = matcher1.group();
                            Pattern pattern2 = Pattern.compile(
                                    "raktáron"
                                    + "|201\\d.\\d{2}.\\d{2}"
                                    + "|a megrendeléstől számított 1-5 munkanap"
                                    + "|a megrendeléstől számított 5-10 munkanap"
                                    + "|a megrendeléstől számított 10-15 munkanap"
                                    + "|a megrendeléstől számított 3-4 hét"
                                    + "|a megrendeléstől számított 4-5 hét"
                                    + "|-- átmeneti készlethiány --"
                                    + "|Keressen a részletekért"
                                    + "|csak előzetes ajánlat alapján"
                                    + "|rendeléskor egyeztetjük"
                                    + "|Kifutó típus, már csak a készlet erejéig szállítható!"
                                    + "|Megszűnt! Már nem tudjuk szállítani!");
                            Matcher matcher2 = pattern2.matcher(inputLine);
                            if (matcher2.find()) {
                                match2 = matcher2.group();
                                if ("Kifutó típus, már csak a készlet erejéig szállítható!".equals(matcher2.group())) {
                                    Pattern pattern3 = Pattern.compile("raktáron");
                                    Matcher matcher3 = pattern3.matcher(inputLine);
                                    if (matcher3.find()) {
                                        match3 = matcher3.group();
                                    }
                                }
                            }

//                            row.append(match1).append(";");
//                            row.append(match2).append(" ");
//                            row.append(match3);
//                            System.R_NEWFILE.println(row);
//                            row.setLength(0);
                            row.append(match2);
                            if (!"".equals(match3)) {
                                row.append(" ").append(match3);
                                match3 = "";
                            }
                            R_NEWFILE.put(match1, row.toString());
                            System.out.println(match1 + ";" + row);
                            row.setLength(0);
                            match2 = "";
                        }

//                        if (count == 12411) {
//                            System.R_NEWFILE.print("The prod at " + count + " is " + prod);
//                            //1265
//                        }
                    }

                } catch (IOException e) {
                    p = 12;
                    //System.err.println("Caught IOException: " + e.getMessage());
                }
            }
        }

//        for (Map.Entry<String, String> entry : R_NEWFILE.entrySet()) {
//            String key = entry.getKey();
//            String value = entry.getValue();
//            System.R_NEWFILE.println(key + ";" + value);
//        }
        String out;
        String lf = "";
        
        Date today = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String now = formatter.format(today);
        
        FileWriter fw = new FileWriter("reduced_elimex" + now + ".csv");
        
        for (HashMap.Entry<String, String> entry : R_NEWFILE.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            out = lf + key + ";" + value;
            lf = "\n";
            fw.write(out);
        }
        fw.close();
    }
}
